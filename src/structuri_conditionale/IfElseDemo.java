package structuri_conditionale;

import java.util.Scanner;

public class IfElseDemo {

    //Citim un numar intreg si daca numarul este > 3, atunci afisam un mesaj
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numar = scanner.nextInt();
        if (numar > 3) {
            System.out.println("Ai introdus un numar mai mare decat 3");
        } else {
            System.out.println("Ati introdus un numar mai mic decat 3");
        }
    }
}
