package structuri_conditionale;

import java.util.Scanner;

public class SwitchDemo {
    public static void main(String[] args) {
        //citim un numar si afisam mesaje diferite in functie valorile urmatoare, daca este 1 sau 2 sau altceva
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduceti numarul: ");
        int n = sc.nextInt();

        switch (n) {
            case 1:
                System.out.println("Ati introdus optiunea 1");
                break;
            case 2:
                System.out.println("Ati introdus optiunea 2");
                break;
            default:
                System.out.println("Ati introdus altceva");
                System.out.println("Print ceva in plus");
        }

        // JDK 12 or newer with enhanced switch
//        switch (n) {
//            case 1 -> System.out.println("Ati introdus optiunea 1");
//            case 2 -> System.out.println("Ati introdus optiunea 2");
//            default -> {
//                System.out.println("Ati introdus altceva");
//                System.out.println("Print ceva in plus");
//            }
//        }
    }
}
