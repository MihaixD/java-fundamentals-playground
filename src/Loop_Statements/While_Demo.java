package Loop_Statements;

import java.util.Scanner;

public class While_Demo {
    public static void main(String[] args) {

        // Citim in mod repetat cate un nr, pana cand primim valoarea 0. De fiecare data cand citim, afisam daca nr este mai mare decat 3 sau nu

        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();

        if (a >3){
            System.out.println("este mai mare ca 3");
        }
      //  while (a <0 && a> 0)

        while ( a!= 0){
            a = scanner.nextInt();
            if (a >3){
                System.out.println("este mai mare ca 3");

            }

        }

    }
}
