package Loop_Statements;

public class For_Loop_Demo {
    public static void main(String[] args) {

        //String array defined and initialized with literal value.
        String[] myWords = {"Cuvant", "masina", "telefon"};

        //Empty String array.
        String[] emptyWords = new String[]{};

        //Basic For-Loop.
        for (int i = 0; i < 3; i++) {
            System.out.println(myWords[i]);

        }
        //Advanced For-Loop.
        for (String word: myWords){
            System.out.println(word);
        }


    }
}
