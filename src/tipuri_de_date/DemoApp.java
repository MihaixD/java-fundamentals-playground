package tipuri_de_date;

public class DemoApp {
    public static void main(String[] args) {

        //primative data type, WITH INIT
        int a = 6;
        boolean b = true;
        char c = 'f';
        float v = 6.15f;
        double h = 12.12;
        byte k = 4;
        long j = 12348L;
        short l = 1;

        //declare without init
        int myInt;
        boolean myBoolean;
        double myDouble;

        //OBJECT WITHOUT INIT
        Object myObject;
        String myString=null;

      //  System.out.println(myInt);
        //intelj will not allow you
        System.out.println(myString);

    }

}
