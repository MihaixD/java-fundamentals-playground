package online_store.model;

// This class represents the products added in a customer's shopping cart.
public class ShoppingCart {
    public static final int MAX_CART_SIZE = 10;
    public Product[] products;


    public ShoppingCart() {
        products = new Product[MAX_CART_SIZE];

    }

    public void printProducts() {
        for (Product p : products) {
            if (p != null) {
                p.printProductDetails();
            }
        }

    }

    public int calculateTotalPrice() {
        int sum = 0;
        for (Product p : products) {
            if (p != null) {
                sum += p.pret;
            }
        }
        return sum;
    }
}
