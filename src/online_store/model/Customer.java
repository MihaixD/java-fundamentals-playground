package online_store.model;


// Example of instance-level fields and instance level methods
public class Customer {
    public String name;
    public String email;
    public int customerId;


    public void printUserDetails() {
        System.out.println("Customer (" +
                "name = " + name + ", " +
                "email = " + email + ", " +
                "customerId = " + customerId +
                ")");

        // We can call both methods from an instance-method (both static and non-static)
        myStaticMethod();
    }


    // We can call static methods from the same class in a non-static method.
    public void myInstanceMethod() {
        printUserDetails();
    }

    // We can NOT call static methods from the same class in a static method.
    public static void myStaticMethod() {
        //printUserDetails();
    }
}
