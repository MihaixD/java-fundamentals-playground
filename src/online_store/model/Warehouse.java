package online_store.model;

// A warehouse holds multiple products
public class Warehouse {
    // Constants
    public static final int MAX_WAREHOUSE_SIZE = 20;

    //Fields
    public Product[] products;

    //Constructors
    public Warehouse (){
        products =  new Product[MAX_WAREHOUSE_SIZE];

    }
    //Methods
    // This method is duplicated from shopping cart
     public void printProducts (){
         for (Product p : products) {
             if (p != null) {
                 p.printProductDetails();
             }
         }
     }
}

