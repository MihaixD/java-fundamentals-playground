package online_store.model;


// Example of static constants - class level constant fields
public class UserMessages {
    // Usually a bad practice - class level fields are normally declared final (constant)
    // public static int x = 0;

    public static final String GREET_USER_MESSAGE = "Welcome to our online store!";
    public static final String GOODBYE_USER_MESSAGE = "Thank you for shopping! Come again soon!";


    public static String welcomeUserMessage(String userName) {
        return "Hi, " + userName + ". " + GREET_USER_MESSAGE;
    }
}
