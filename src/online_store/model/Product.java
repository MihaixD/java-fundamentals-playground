package online_store.model;


// A product has a title, description, price.
public class Product {

    //Fields/Campuri

    public String titlu;
    public String descriere;
    public double pret;

    //Constructors

    public Product (String titlu, String descriere, double pret){
        this.titlu = titlu;
        this.descriere = descriere;
        this.pret = pret;
    }


    //Methods

    public Product(){
        System.out.println("S-a apelat constructorul gol");
    }
    public void printProductDetails(){
        System.out.println("Product.titlu: " + titlu +
                ", Product.descriere: " + descriere +
                " Product.pret: " + pret);
    }

}
