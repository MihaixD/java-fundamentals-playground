package online_store;

import online_store.model.Product;
import online_store.model.Warehouse;

public class DemoWarehouse {
    public static void main(String[] args) {
        Warehouse  warehouse = new Warehouse();
        warehouse.products[0] = new Product("Laptop","DEL",200);
        warehouse.products[2] = new Product("Laptop","ASUS",400);


        warehouse.products[7] = warehouse.products[0];
        // Update one reference
        warehouse.products[0].pret = 500;

        // Both positions get updated, because it's actually only one object.
        warehouse.printProducts();
    }
}
