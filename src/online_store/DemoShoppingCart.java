package online_store;

import online_store.model.Product;
import online_store.model.ShoppingCart;

public class DemoShoppingCart {
    public static void main(String[] args) {
        ShoppingCart cart = new ShoppingCart();
        cart.products[0] = new Product("Laptop","DEL",200);
        cart.products[2] = new Product("Laptop","ASUS",400);
        // Intellij will not recommend you to use negative values for array index
        // cart.products[-1] = new Product();
        // Index array out of bounds
        // cart.products[10] = new Product();

        // NullPointerException ( didn't initialize an object)
        // System.out.println(cart.products[7].titlu);

        cart.printProducts();
        System.out.println("The total price of the products is: " + cart.calculateTotalPrice());
    }
}
