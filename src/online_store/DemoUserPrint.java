package online_store;

import online_store.model.Customer;

public class DemoUserPrint {
    public static void main(String[] args) {
        Customer customer = new Customer();
        // Empty, uninitialized user
        customer.printUserDetails();

        // The method is NOT static, it can not be called on class level
        //Customer.printUserDetails()

        customer.customerId = 1;
        customer.name = "John";
        customer.email = "john@domain.com";
        // Customer with some values in fields
        customer.printUserDetails();

    }
}
