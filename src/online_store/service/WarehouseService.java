package online_store.service;

import online_store.model.Product;
import online_store.model.Warehouse;

// This class is responsible for checking is products within a warehouse are available.
public class WarehouseService {

    // How do we check if two products are "the same" - what does "equals" mean?
    public boolean isProductAvailable(Warehouse warehouse, Product product) {
        // example 1 - identical object - the same actual object in memory
        //warehouse.products[0] == product

        // example 2 - identical data
        //warehouse.products[0].titlu.equals(product.titlu)

        // example 3? - check multiple fields, not just title?

        // example 4 - add ID (public int id;) field in Product class
        // warehouse.products[0].id = product.id

        return false;
    }
}
