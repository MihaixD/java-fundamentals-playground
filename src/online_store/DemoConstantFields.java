package online_store;

import online_store.model.UserMessages;

public class DemoConstantFields {
    public static void main(String[] args) {
        System.out.println("This is the hello message for users:");
        System.out.println(UserMessages.GREET_USER_MESSAGE);

        System.out.println("Example of greeting user by name:");
        System.out.println(UserMessages.welcomeUserMessage("John"));

        // Other examples from Java util package
        System.out.println("Example of constant in Math: Math.PI = " + Math.PI);
        System.out.println("Example of static method in Math: Math.pow(2, 3) = " + Math.pow(2, 3));
    }
}
