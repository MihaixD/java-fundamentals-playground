package online_store;

import online_store.model.Product;

public class DemoProduct {
    public static void main(String[] args) {
        Product myProduct = new Product();
        myProduct.titlu = "Laptop";
        myProduct.descriere = "IntelCore";
        myProduct.pret = 200;

        myProduct.printProductDetails();

        //Initialize with constructor with paramiters

        Product mySecondProduct = new Product("laptop","HP", 100);
        mySecondProduct.printProductDetails();
    }
}
